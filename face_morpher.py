import cv2
import numpy as np
import dlib

import os, sys
import subprocess
import numpy as np
import scipy.io as sio
from skimage import io
from time import time
import matplotlib.pyplot as plt

#sys.path.append('..')
import face3d
from face3d import mesh
from face3d.morphable_model import MorphabelModel

#import copy


# --- 1. load model
bfm = MorphabelModel('examples/Data/BFM/Out/BFM.mat')
print('init bfm model success')

# --- 2. generate face mesh: vertices(represent shape) & colors(represent texture)
sp = bfm.get_shape_para('random')
ep = bfm.get_exp_para('random')
vertices = bfm.generate_vertices(sp, ep)

tp = bfm.get_tex_para('random')
colors = bfm.generate_colors(tp)
colors = np.minimum(np.maximum(colors, 0), 1)


# --- 3. transform vertices to proper position
s = 8e-04
#angles = [10, 30, 20]
angles = [0, 0, 0]
t = [0, 0, 0]
transformed_vertices = bfm.transform(vertices, s, angles, t)
projected_vertices = transformed_vertices.copy() # using stantard camera & orth projection


#cap = cv2.VideoCapture(0)
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("examples/shape_predictor_68_face_landmarks.dat")



def detect_once(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = detector(gray)
    pts = []
    x1, y1, x2, y2 = 0, 0, 0, 0
    x = np.zeros((68, 2))
    is_face = True

    if len(faces) ==0:
        is_face = False

    else:
        for face in faces:

            x1 = face.left()
            y1 = face.top()
            x2 = face.right()
            y2 = face.bottom()
            #cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 3)

            landmarks = predictor(gray, face)

            for n in range(0, 68):
                x = landmarks.part(n).x - x1
                y = landmarks.part(n).y - y1
                pts.append([x, y])

                #print("lpx, lpy,", landmarks.part(n).x, landmarks.part(n).y)
                #print("x1, y1,", x1, y1)
                #print("x, y,", x, y)
                #print("-----")
            break
        
        x = pts

    return x, x1, y1, x2, y2, is_face



def detection_pipe():


    while True:
        _, frame = cap.read()
        #detect_once(frame)


        # im = cv2.imread("test3.jpg")
        x, x1, y1, x2, y2, is_face = detect_once(frame)
        if not is_face:
            continue

        # reverse y axis
        y_vec = np.zeros((68, 1)) + (y2 - y1) #y_max
        x_vec = np.zeros((68, 1)) + (x2 - x1)
        max_vec = np.concatenate([x_vec, y_vec], axis = 1)
        #print(" x1, y1, x2, y2", x1, y1, x2, y2)
        w = x2 - x1
        h = y2 - y1
        x = max_vec -x

        # resize
        x[:,1] -= ( h//2)
        x[:,0] -= ( w//2)
        x = x * 0.6


        X_ind = bfm.kpt_ind # index of keypoints in 3DMM. fixed.
        
        # fit
        fitted_sp, fitted_ep, fitted_s, fitted_angles, fitted_t = bfm.fit(x, X_ind, max_iter = 3)
        
        # verify fitted parameters
        fitted_vertices = bfm.generate_vertices(fitted_sp, fitted_ep)
        transformed_vertices = bfm.transform(fitted_vertices, fitted_s, fitted_angles, fitted_t)
        
        image_vertices = mesh.transform.to_image(transformed_vertices, h, w)
        fitted_image = mesh.render.render_colors(image_vertices, bfm.triangles, colors, h, w)
        
        fitted_image = cv2.cvtColor(fitted_image, cv2.COLOR_RGB2BGR)
        



        cv2.imshow("Frame", fitted_image)
        key = cv2.waitKey(1)
        if key == 27:
            break



def morphing(frame):
    try:
        fitted_image = "-1"

        x, x1, y1, x2, y2, is_face = detect_once(frame)
        if not is_face:
            return False, "-1"


        # reverse y axis
        y_vec = np.zeros((68, 1)) + (y2 - y1) #y_max
        x_vec = np.zeros((68, 1)) + (x2 - x1)
        max_vec = np.concatenate([x_vec, y_vec], axis = 1)
        #print(" x1, y1, x2, y2", x1, y1, x2, y2)
        w = x2 - x1
        h = y2 - y1
        x = max_vec -x

        # resize
        x[:,1] -= ( h//2)
        x[:,0] -= ( w//2)
        x = x * 0.6


        X_ind = bfm.kpt_ind # index of keypoints in 3DMM. fixed.

        # fit
        fitted_sp, fitted_ep, fitted_s, fitted_angles, fitted_t = bfm.fit(x, X_ind, max_iter = 3)

        # verify fitted parameters
        fitted_vertices = bfm.generate_vertices(fitted_sp, fitted_ep)
        transformed_vertices = bfm.transform(fitted_vertices, fitted_s, fitted_angles, fitted_t)
        image_vertices = mesh.transform.to_image(transformed_vertices, h, w)
        fitted_image = mesh.render.render_colors(image_vertices, bfm.triangles, colors, h, w)
        # cvt color
        fitted_image = cv2.cvtColor(fitted_image, cv2.COLOR_RGB2BGR)

    except:
        fitted_image = "-1"
        print(datetime.now().strftime("%Y/%m/%d %H:%M:%S") ,"error in morphing()")
        return False, "-1"

    finally:
        return True, fitted_image


if __name__ == "__main__":
    
    detection_pipe()


