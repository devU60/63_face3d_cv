# -*- coding:utf-8 -*-
import base64

#import cv2

#file読み込み
#file = open('examples/test2.jpg', 'rb').read()
file = open('chrome.png', 'rb').read()


#base64でencode
enc_file = base64.b64encode( file )
#encodeしたascii文字列を出力
print(enc_file)

#decodeしてもとデータに変換
dec_file = base64.b64decode( enc_file )
#cv2.imwrite("_test.png", dec_file)

#decodeしたデータと元データを比較
if file == dec_file :
    print('SAME')
else :
    print('NOT SAME')
