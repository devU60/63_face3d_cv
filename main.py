import os
from flask import Flask, render_template
from werkzeug.contrib.fixers import ProxyFix
from flask import request, make_response, jsonify
import werkzeug
from datetime import datetime

import face_morpher
from PIL import Image
import base64
from io import BytesIO
import numpy as np

app = Flask(__name__,static_folder=None)

@app.route('/post/image', methods=['POST'])
def get_morph():
    #try:
        received = request.get_json()
        print(received)
        r_base64 = received["image"]
        print("r_base64", r_base64, type(r_base64))

        # bytes to image
        r_base64 = bytes(r_base64, encoding='utf-8', errors='replace')
        r_image = base64.b64decode(r_base64)
        r_image = Image.open(BytesIO(r_image))
        r_image = np.asarray(r_image)[:,:,::-1]
        # image to face image
        ret, morph_image = face_morpher.morphing(r_image)
        print(morph_image)   

        # image to bytes
        imgByteArr = BytesIO()
        im = Image.fromarray(np.uint8(morph_image))
        im.save(imgByteArr, format='PNG')
        im = imgByteArr.getvalue()
        print(im)
        morph_base64 = base64.b64encode(morph_image)
       
        # bytes to str
        print(morph_base64)
        morph_base64 = morph_base64.decode("ascii")
        ret = jsonify({'success': ret,'image': morph_base64})
    #except:
    #    print(datetime.now().strftime("%Y/%m/%d %H:%M:%S") ,"exception in get_morph():")
    #    ret = jsonify({'success':False,'image':"-1"})

    #finally:
        #ret = jsonify({'success':False,'image':"-1"})
        return make_response(ret)


app.wsgi_app = ProxyFix(app.wsgi_app)
app.debug = bool(os.environ.get('PRODUCTION'))
 
if __name__ == '__main__':
    print(app.url_map)
    app.run(host="0.0.0.0",debug=True, port = 51249)
