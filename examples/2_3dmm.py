''' 3d morphable model example
3dmm parameters --> mesh 
fitting: 2d image + 3dmm -> 3d face
'''
import os, sys
import subprocess
import numpy as np
import scipy.io as sio
from skimage import io
from time import time
import matplotlib.pyplot as plt

sys.path.append('..')
import face3d
from face3d import mesh
from face3d.morphable_model import MorphabelModel

# --------------------- Forward: parameters(shape, expression, pose) --> 3D obj --> 2D image  ---------------
# --- 1. load model
bfm = MorphabelModel('Data/BFM/Out/BFM.mat')
print('init bfm model success')

# --- 2. generate face mesh: vertices(represent shape) & colors(represent texture)
sp = bfm.get_shape_para('random')
ep = bfm.get_exp_para('random')
vertices = bfm.generate_vertices(sp, ep)
print("sp", sp.shape)
print("ep", ep.shape)
print("vertices", vertices.shape)

tp = bfm.get_tex_para('random')
colors = bfm.generate_colors(tp)
colors = np.minimum(np.maximum(colors, 0), 1)
print("tp", tp.shape)
print("colors", colors.shape)


# --- 3. transform vertices to proper position
s = 8e-04
#angles = [10, 30, 20]
angles = [0, 0, 50] # rotation by x, y, z(z means depth)
t = [0, 0, 0] # x, y, openness of mouth
transformed_vertices = bfm.transform(vertices, s, angles, t)
projected_vertices = transformed_vertices.copy() # using stantard camera & orth projection
print("transformed_vertices", transformed_vertices.shape)
print("projected_vertices", projected_vertices.shape)


# --- 4. render(3d obj --> 2d image)
# set prop of rendering
h = w = 256; c = 3
image_vertices = mesh.transform.to_image(projected_vertices, h, w)
image = mesh.render.render_colors(image_vertices, bfm.triangles, colors, h, w)
print("image_vertices", image_vertices.shape)
print("image", image.shape)

    
print("bfm.kpt_ind", bfm.kpt_ind.shape)
# -------------------- Back:  2D image points and corresponding 3D vertex indices-->  parameters(pose, shape, expression) ------
## only use 68 key points to fit
x = projected_vertices[bfm.kpt_ind, :2] # 2d keypoint, which can be detected from image
print(x)



import cv2
import numpy as np
import dlib
import copy

x = copy.deepcopy(x)


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")


def detect_once(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = detector(gray)
    pts = []

    for face in faces:

        x1 = face.left()
        y1 = face.top()
        x2 = face.right()
        y2 = face.bottom()
        #cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 3)

        landmarks = predictor(gray, face)

        for n in range(0, 68):
            x = landmarks.part(n).x - x1
            y = landmarks.part(n).y - y1
            pts.append([x, y])

            print("lpx, lpy,", landmarks.part(n).x, landmarks.part(n).y)
            print("x1, y1,", x1, y1)
            print("x, y,", x, y)
            print("-----")
        break

    return np.array(pts), x1, y1, x2, y2



## read loop





#im = cv2.imread("test.png")
#im = cv2.imread("test2.jpg")
im = cv2.imread("test3.jpg")
face_x, x1, y1, x2, y2 = detect_once(im)
#x = x + 100
#face_x = x
#x = x/100.0
x = face_x 

print("----")
print(face_x)

# invert y
# x_center = np.mean(x, axis=0)
# y_center = np.mean(x, axis=1)
#y_max = x[:,1]
#y_max = np.amax(y_max)
#print("y_max", y_max)
y_vec = np.zeros((68, 1)) + (y2 - y1) #y_max
#print(y_max.shape)
#sub = np.zeros(y_max.shape)
#print(sub.shape)
#sub = np.concatenate([sub, y_max], axis = 1)
#print(sub.shape)

# invert x
#x_vec =np.zeros((68, 1))
x_vec = np.zeros((68, 1)) + (x2 - x1)
max_vec = np.concatenate([x_vec, y_vec], axis = 1)

print(" x1, y1, x2, y2", x1, y1, x2, y2)
w = x2 - x1
h = y2 - y1
x = max_vec -x

x[:,1] -= ( h//2)
x[:,0] -= ( w//2)

x = x * 0.6

frame = np.zeros((1000,1000,3), dtype=np.uint8)
for n in range(0, 68):
    xx = x[n]
    x_ = xx[0]
    y_ = xx[1]
    cv2.circle(frame, (int(x_), int(y_)), 4, (255, 0, 0), -1)

cv2.imwrite("out_face3d.png", frame)
#mean_x = np.mean(x)
#std_x = np.std(x)
#x = x - mean_x
#x = x / std_x
print(x)

X_ind = bfm.kpt_ind # index of keypoints in 3DMM. fixed.
print("x", x.shape)
print("X_ind", X_ind.shape)

# fit
fitted_sp, fitted_ep, fitted_s, fitted_angles, fitted_t = bfm.fit(x, X_ind, max_iter = 3)
print("fitted_sp", fitted_sp[0].shape)
print("fitted_ep", fitted_ep[0].shape)
print("fitted_s", fitted_s)
print("fitted_angles", fitted_angles)
print("fitted_t", fitted_t)

# verify fitted parameters
fitted_vertices = bfm.generate_vertices(fitted_sp, fitted_ep)
transformed_vertices = bfm.transform(fitted_vertices, fitted_s, fitted_angles, fitted_t)
print("fitted_vertices", fitted_vertices.shape)
print("transformed_vertices", transformed_vertices.shape)


image_vertices = mesh.transform.to_image(transformed_vertices, h, w)
fitted_image = mesh.render.render_colors(image_vertices, bfm.triangles, colors, h, w)
print("image_vertices", image_vertices.shape)
print("fitted_image", fitted_image.shape)


# ------------- print & show 
print('pose, groudtruth: \n', s, angles[0], angles[1], angles[2], t[0], t[1])
print('pose, fitted: \n', fitted_s, fitted_angles[0], fitted_angles[1], fitted_angles[2], fitted_t[0], fitted_t[1])

save_folder = 'results/3dmm'
if not os.path.exists(save_folder):
    os.mkdir(save_folder)

io.imsave('{}/generated.jpg'.format(save_folder), image)
io.imsave('{}/fitted.jpg'.format(save_folder), fitted_image)














"""

### ----------------- visualize fitting process
# fit
fitted_sp, fitted_ep, fitted_s, fitted_angles, fitted_t = bfm.fit(x, X_ind, max_iter = 3, isShow = True)

# verify fitted parameters
for i in range(fitted_sp.shape[0]):
	fitted_vertices = bfm.generate_vertices(fitted_sp[i], fitted_ep[i])
	transformed_vertices = bfm.transform(fitted_vertices, fitted_s[i], fitted_angles[i], fitted_t[i])

	image_vertices = mesh.transform.to_image(transformed_vertices, h, w)
	fitted_image = mesh.render.render_colors(image_vertices, bfm.triangles, colors, h, w)
	io.imsave('{}/show_{:0>2d}.jpg'.format(save_folder, i), fitted_image)

options = '-delay 20 -loop 0 -layers optimize' # gif. need ImageMagick.
subprocess.call('convert {} {}/show_*.jpg {}'.format(options, save_folder, save_folder + '/3dmm.gif'), shell=True)
subprocess.call('rm {}/show_*.jpg'.format(save_folder), shell=True)
"""