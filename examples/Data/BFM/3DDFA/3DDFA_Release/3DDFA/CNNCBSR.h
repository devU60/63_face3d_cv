#ifndef CNNCBSR_H
#define CNNCBSR_H

#include <windows.h>

#define STD_SIZE 100
#define INPUT_LENGTH (STD_SIZE)*(STD_SIZE)*8
#define JFEATURE_LENGTH 234  

DECLARE_HANDLE(HCNNCBSR);

HCNNCBSR CNNCBSRCreate(int net_ind);
void CNNCBSRFree(HCNNCBSR handle);
void CNNCBSRRun(HCNNCBSR handle, float* image, int width, int height, int nChannels,
	float* feature);
#endif
