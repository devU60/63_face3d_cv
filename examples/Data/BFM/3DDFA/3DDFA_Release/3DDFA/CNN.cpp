// Test.cpp : 定义控制台应用程序的入口点。
//

#include <stdio.h>
#include <string>
#include <windows.h>
#include <iostream>
#include <fstream>

using namespace std;

#include "CNNCBSR.h"
#pragma comment(lib, "../bin/CNNCBSR.lib")

int main(int argc, char* argv[])
{

	ifstream in(argv[1]);

	ifstream in_file;

	int net_ind;
	sscanf_s(argv[2],"%d",&net_ind);

	HCNNCBSR HCNNCBSR = CNNCBSRCreate(net_ind);

	string iter_ind;
	string shapeName;
	string fileName;


	float input[INPUT_LENGTH];
	while (in >> shapeName) {
		// load image
		fileName = shapeName.substr(0, shapeName.length());

		cout << "process " << fileName << "\n";

		in_file.open(fileName, ios_base::binary);
		int size;
		if (in_file) {
			in_file.read((char*)&size, sizeof(int));
			in_file.read((char*)input, size * sizeof(float));
			in_file.close();
		} else {
			printf("load %s failed\n", fileName);
			return 0;
		}


		// extract feature 
		float feature[JFEATURE_LENGTH];
		CNNCBSRRun(HCNNCBSR, input, STD_SIZE, STD_SIZE, 8, 
			feature);

		// save
		string feaName = fileName + ".dat";
		ofstream out(feaName.c_str(), ios_base::binary);
		out.write((const char*)feature, JFEATURE_LENGTH * sizeof(float));
		out.close();

	}


	CNNCBSRFree(HCNNCBSR);

	return 1;
}
